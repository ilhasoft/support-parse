# Ilhasoft Support Parse CHANGELOG

## Version 0.4.5 (2017-06-23)

* Fix `ErrorMessageDecoder` class

## Version 0.4.4 (2017-06-20)

* Implemented new way of handling response errors
* Rotate thumbnail if needed

## Version 0.4.3 (2017-05-28)

* Fix **selectKeys** extension for `ParseQuery`

## Version 0.4.2 (2017-05-14)

* Update `SafeParseDelegate` implementation
* Update libraries and tools
* Added `MapParseDelegate` and `StringParseDelegate`
* Fix nullable delegates (JSONObject and JSONArray)
* Add all `ParseQuery` where\* methods
* Changes for rx.Observable deprecated methods
* Implemented a custom ErrorMessageDecoder interface for get friendly messages
* Update projet to use community parse library again
* Update CHANGELOG

## Version 0.4.0 (2017-04-24)

* Add kotlin version
  * Extensions for `ParseQuery`
  * Delegates for `ParseObject` attributes
* Update Facebook SDK

## Version 0.3.3 (2017-04-21)

* Update core dependencies

## Version 0.3.2 (2017-03-16)

* Refactor strings messages with design validations

## Version 0.3.1 (2017-02-15)

* Replace emptyList for instance creation on setting a list when there is no result

## Version 0.3.0 (2017-02-09)

* Refactor project to depend on core submodules instead of the whole project

## Version 0.2.12 (2016-06-17)

* Add method to clear page results
* Add operation to change the whole list instead of add last results
* Fix fetch method to notifyDataSetChanged with stableIds
* Fix stable ids after registered loader
* Add verification to don't clear all objects even when notifyAll is true
* Add all object without default adapter notification
* Remove clear objects call on fetch method
* Update support-core reference to tag 0.7.0
* Fix fetch method when there is no data anymore

## Version 0.2.10 (2016-12-06)

* Update parse media to support parent ParseFile
* Fix use of wrong ParseFile implementation
* Add parcelable implementation for ParseMedia

## Version 0.2.7 (2016-12-02)

* Add new media types on ParseMedia

## Version 0.2.6 (2016-11-23)

* Add facebook query on data
* Add media helper to create thumbnail and save medias
* Add support for java 8 on sample module
* Add installation helper to save user after login
* Add parse file helper with create methods
* Update support core version to 0.6.0
* Update parse platform to newest version to fix file crash

## Version 0.2.4 (2016-10-11)

* Update parse version to 1.13.1

## Version 0.2.3 (2016-09-30)

* Fixed bugs on Parse adapters

## Version 0.2.2 (2016-07-15)

* Fixed bugs on Parse adapters

## Version 0.2.1 (2016-06-25)

* Added some util methods to SimpleParseListAdapter

## Version 0.2.0 (2016-06-17)

* Added SimpleParseListAdapter for simple use cases;
* Implemented AutoParseQueryAdapter;
* Updated support core library;
* Implemented basic ParseObjects that implement toString, equals and hashCode;
* Added changelog;

## Version 0.1.4 (2016-06-08)

* Add hybrid save listener for both signup and save callbacks;

## Version 0.1.3 (2016-05-30)

* Updated support core library;

## Version 0.1.2 (2016-05-29)

* Fixed build on some projects (duplicated dependences of bolts libraries);
* Fixed `DeleteListener` and renamed `RequestResponseHelper` to `ResponseHelper`;
* Added **hasAnyError** method to `ResponseHelper`;

## Version 0.1.1 (2016-05-26)

* Fixed duplicated entry and update support core library;
* Adjust listener method names;

## Version 0.1.0 (2016-05-23)

* **Initial release**;
* Created awesome classes:
    * `ParseQueryAdapter` to easy implement recycler adapters with less code, and with useful features;
    * `LocalRequestHelper` allow to query locally and remotely;
    * `RequestResponseHelper` provide so easy friendly error messages when there is;
    * `SelectParseObjectDialog` is a simple way to select a **ParseObject** from a query;
* Created useful listeners for easy handler parse callbacks;