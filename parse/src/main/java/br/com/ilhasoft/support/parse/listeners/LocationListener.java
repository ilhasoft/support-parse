package br.com.ilhasoft.support.parse.listeners;

import com.parse.LocationCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class LocationListener extends Listener2<ParseGeoPoint> implements LocationCallback {

    @Override
    public final void done(ParseGeoPoint geoPoint, ParseException e) {
        this.doneInternal(geoPoint, e);
    }

}
