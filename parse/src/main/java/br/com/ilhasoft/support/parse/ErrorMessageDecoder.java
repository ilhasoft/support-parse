package br.com.ilhasoft.support.parse;

import android.content.Context;
import android.support.annotation.StringRes;

import com.parse.ParseException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by daniel on 14/05/17.
 */
public class ErrorMessageDecoder {

    @StringRes
    protected static final int NO_ERROR = -837456;
    private static ErrorMessageDecoder sInstance = null;

    protected final Context context;

    public static void initialize(Context context) {
        sInstance = new ErrorMessageDecoder(context);
    }

    public static void initialize(ErrorMessageDecoder decoder) {
        sInstance = decoder;
    }

    public static ErrorMessageDecoder instance() {
        return sInstance;
    }

    protected ErrorMessageDecoder(Context context) {
        this.context = context;
    }

    public final void decode(ParseException e, OnErrorMessageCallback callback) {
        decode(e, false, callback);
    }

    public final void decode(ParseException e, boolean isLogin, OnErrorMessageCallback callback) {
        final int messageId = getMessageId(e, isLogin);
        if (messageId != NO_ERROR) {
            String message = context.getString(messageId);
            if (messageId == R.string.error_message_unexpected_error) {
                message = getMessage(e, isLogin, message);
            }

            callback.onErrorMessage(message);
        }
    }

    protected String getMessage(ParseException e, boolean isLogin, String foundMessage) {
        if (e.getCode() == ParseException.SCRIPT_ERROR) {
            try {
                final JSONObject jsonObject = new JSONObject(e.getMessage());
                final String defaultErrorMessage = jsonObject.optString("default_error_message");
                if (!defaultErrorMessage.isEmpty()) {
                    return defaultErrorMessage;
                }
            } catch (JSONException ignored) { }
        }
        return foundMessage;
    }

    @StringRes
    private int getMessageId(ParseException e, boolean isLogin) {
        if (e == null) return NO_ERROR;
        switch (e.getCode()) {
            case ParseException.INVALID_EMAIL_ADDRESS:
                return R.string.error_message_invalid_email_address;
            case ParseException.EMAIL_TAKEN:
            case ParseException.USERNAME_TAKEN:
                return R.string.error_message_email_taken;
            case ParseException.EMAIL_NOT_FOUND:
                return R.string.error_message_email_not_found;
            case ParseException.CONNECTION_FAILED:
                return R.string.error_message_connection_failed;
            case ParseException.OBJECT_NOT_FOUND:
                return isLogin ? R.string.error_message_user_not_found : R.string.error_message_object_not_found;
            case ParseException.CACHE_MISS:
                return NO_ERROR;
            default:
                return R.string.error_message_unexpected_error;
        }
    }

    public interface OnErrorMessageCallback {
        void onErrorMessage(CharSequence message);
    }

}
