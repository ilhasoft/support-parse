package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.SignUpCallback;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class SignUpListener extends Listener1 implements SignUpCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
