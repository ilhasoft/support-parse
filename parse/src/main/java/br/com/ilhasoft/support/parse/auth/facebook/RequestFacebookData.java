package br.com.ilhasoft.support.parse.auth.facebook;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;

import org.json.JSONObject;

import io.reactivex.Flowable;

/**
 * Created by john-mac on 11/23/16.
 */
public class RequestFacebookData {

    public static final String DEFAULT_FIELDS = "id, name, email, gender, birthday, picture.type(large)";

    public static Flowable<JSONObject> getFacebookUserInformation() {
        return getFacebookUserInformation(DEFAULT_FIELDS);
    }

    /**
     * Request user information from Facebook
     * @param fields Facebook user fields separated by comma (Ex. id, name, email, gender)
     * @return JSONObject with data collected
     */
    public static Flowable<JSONObject> getFacebookUserInformation(final String fields) {
        return Flowable.fromCallable(() -> {
            final GraphRequest request;
            final Bundle parameters = new Bundle();
            request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), null);
            parameters.putString("fields", fields);
            request.setParameters(parameters);
            return request.executeAndWait().getJSONObject();
        });
    }

}
