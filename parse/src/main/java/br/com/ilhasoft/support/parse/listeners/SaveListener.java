package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.SaveCallback;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class SaveListener extends Listener1 implements SaveCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
