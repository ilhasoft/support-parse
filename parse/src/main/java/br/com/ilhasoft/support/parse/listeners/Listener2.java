package br.com.ilhasoft.support.parse.listeners;

import android.support.annotation.StringRes;

import com.parse.ParseException;

import br.com.ilhasoft.support.parse.ResponseHelper;

/**
 * Class which checks if there was an error and calls the appropriate method already
 * with a friendly message if necessary.
 *
 * Created by daniel on 23/05/16.
 */
abstract class Listener2<T> {

    protected void doneInternal(T object, ParseException e) {
        this.onFinished();

        @StringRes final int errorMessageId = ResponseHelper.getErrorMessageId(e);
        if (errorMessageId == ResponseHelper.NO_ERROR) {
            this.onSuccess(object);
        } else {
            this.onError(e, errorMessageId);
        }
    }

    /**
     * Called when the request is finishing. This is where most common code between
     * the error and success should go. It is called before the {@link Listener2#onSuccess}
     * or {@link Listener2#onError}
     */
    public void onFinished() { }

    /**
     * Called when the request was successfully completed.
     *
     * @param t Generally the results of the operation.
     */
    public abstract void onSuccess(T t);

    /**
     * Called when the request was finished with an error.
     *
     * @param e Generally an {@link Throwable} that was thrown by the operation.
     * @param errorMessageId A resource id to a friendly error message that can be
     *                       shown to the end user.
     */
    public abstract void onError(ParseException e, @StringRes int errorMessageId);

}
