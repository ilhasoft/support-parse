package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * Created by John Cordeiro on 08/06/16.
 */
public abstract class HybridSaveListener extends Listener1 implements SignUpCallback, SaveCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
