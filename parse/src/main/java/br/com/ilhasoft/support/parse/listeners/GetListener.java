package br.com.ilhasoft.support.parse.listeners;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class GetListener<T extends ParseObject> extends Listener2<T> implements GetCallback<T> {

    @Override
    public final void done(T object, ParseException e) {
        this.doneInternal(object, e);
    }

}
