package br.com.ilhasoft.support.parse.widgets;

import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by daniel on 31/03/16.
 */
public interface ParseQueryFactory<T extends ParseObject> {

    ParseQuery<T> create();

}
