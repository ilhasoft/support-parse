package br.com.ilhasoft.support.parse;

import com.parse.CountCallback;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by johncordeiro on 11/11/15.
 */
public final class LocalRequestHelper {

    private static final String TAG = "LocalRequestHelper";

    private LocalRequestHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static <T extends ParseObject> void findInBackgroundLocalThenNetwork(final ParseQuery<T> query, final FindCallback<T> callback) {
        query.cancel();
        findInBackgroundLocal(query, new FindCallback<T>() {
            @Override
            public void done(final List<T> cacheData, ParseException exception) {
                callback.done(cacheData, exception);
                findInBackgroundNetwork(query, new FindCallback<T>() {
                    @Override
                    public void done(List<T> networkData, ParseException exception) {
                        callback.done(networkData, exception);
                        if (exception == null)
                            updateLocalStorage(cacheData, networkData);
                    }
                });
            }
        });
    }

    private static <T extends ParseObject> void findInBackgroundLocal(final ParseQuery<T> query, final FindCallback<T> callback) {
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<T>() {
            @Override
            public void done(List<T> list, ParseException exception) {
                if (exception == null && (list == null || list.isEmpty())) {
                    exception = new ParseException(ParseException.CACHE_MISS, "Cache miss");
                }
                callback.done(list, exception);
            }
        });
    }

    public static <T extends ParseObject> void findInBackgroundNetwork(final ParseQuery<T> query, final FindCallback<T> callback) {
        ParseQuery<T> queryFromNetwork = enableFromNetwork(query);
        if (queryFromNetwork != null) {
            queryFromNetwork.findInBackground(new FindCallback<T>() {
                @Override
                public void done(final List<T> newData, ParseException exception) {
                    callback.done(newData, exception);
                }
            });
        }
    }

    public static <T extends ParseObject> void countInBackgroundLocalThenNetwork(final ParseQuery<T> query, final CountCallback callback) {
        query.cancel();
        countInBackgroundLocal(query, new CountCallback() {
            @Override
            public void done(int count, ParseException e) {
                callback.done(count, e);
                countInBackgroundNetwork(query, new CountCallback() {
                    @Override
                    public void done(int count, ParseException e) {
                        callback.done(count, e);
                    }
                });
            }
        });
    }

    private static <T extends ParseObject> void countInBackgroundLocal(final ParseQuery<T> query, final CountCallback callback) {
        query.fromLocalDatastore();
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int count, ParseException exception) {
                callback.done(count, exception);
            }
        });
    }

    public static <T extends ParseObject> void countInBackgroundNetwork(final ParseQuery<T> query, final CountCallback callback) {
        ParseQuery<T> queryFromNetwork = enableFromNetwork(query);
        if (queryFromNetwork != null) {
            queryFromNetwork.countInBackground(new CountCallback() {
                @Override
                public void done(int count, ParseException exception) {
                    callback.done(count, exception);
                }
            });
        }
    }

    private static <T extends ParseObject> void updateLocalStorage(final List<T> cacheData, final List<T> networkData) {
        ParseObject.unpinAllInBackground(cacheData, new DeleteCallback() {
            @Override
            public void done(ParseException exception) {
                ParseObject.pinAllInBackground(networkData);
            }
        });
    }

    private static <T extends ParseObject> ParseQuery<T> enableFromNetwork(ParseQuery<T> query) {
        try {
            Method method = query.getClass().getDeclaredMethod("fromNetwork");
            method.setAccessible(true);
            return (ParseQuery<T>) method.invoke(query);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

}
