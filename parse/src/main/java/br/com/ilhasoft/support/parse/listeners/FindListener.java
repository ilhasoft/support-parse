package br.com.ilhasoft.support.parse.listeners;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class FindListener<T extends ParseObject> extends Listener2<List<T>> implements FindCallback<T> {

    @Override
    public final void done(List<T> objects, ParseException e) {
        this.doneInternal(objects, e);
    }

}
