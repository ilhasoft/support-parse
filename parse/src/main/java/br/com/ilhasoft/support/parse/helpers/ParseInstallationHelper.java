package br.com.ilhasoft.support.parse.helpers;

import com.parse.ParseInstallation;
import com.parse.ParseUser;

import io.reactivex.Observable;
import rx.parse2.ParseObservable;

/**
 * Created by john-mac on 11/23/16.
 */
public final class ParseInstallationHelper {

    private ParseInstallationHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static Observable<ParseInstallation> updateInstallationWithUser(ParseUser user) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("user", user);
        return ParseObservable.save(installation);
    }

}
