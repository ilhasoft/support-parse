package br.com.ilhasoft.support.parse.helpers;

import android.graphics.Bitmap;

import com.parse.ParseFile;

import java.io.ByteArrayOutputStream;

import io.reactivex.Flowable;

/**
 * Created by john-mac on 9/24/16.
 */
public final class ParseFileHelper {

    private ParseFileHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static Flowable<ParseFile> getParseFileFromBitmap(Bitmap imageBitmap) {
        return Flowable.fromCallable(() -> {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return new ParseFile("image_file.png", byteArrayOutputStream.toByteArray());
        });
    }

}
