package br.com.ilhasoft.support.parse.listeners;

import com.parse.LogOutCallback;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class LogOutListener extends Listener1 implements LogOutCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
