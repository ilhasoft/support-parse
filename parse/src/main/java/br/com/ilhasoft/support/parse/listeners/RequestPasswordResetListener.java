package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.RequestPasswordResetCallback;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class RequestPasswordResetListener extends Listener1 implements RequestPasswordResetCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
