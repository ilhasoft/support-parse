package br.com.ilhasoft.support.parse.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.parse.ParseFile;

import java.io.File;

import br.com.ilhasoft.support.core.helpers.IoHelper;
import br.com.ilhasoft.support.graphics.BitmapCompressor;
import br.com.ilhasoft.support.graphics.BitmapHelper;
import br.com.ilhasoft.support.parse.models.ParseMedia;
import br.com.ilhasoft.support.rxgraphics.FileCompressor;
import io.reactivex.Observable;
import rx.parse2.ParseObservable;

/**
 * Created by john-mac on 11/23/16.
 */
public final class MediaHelper {

    private static final String TAG = "MediaHelper";

    private static final int DEFAULT_THUMBNAIL_SIZE = 400;
    private static final int DEFAULT_JPEG_QUALITY = 50;

    private MediaHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    /**
     * Method used to save media with thumbnail if needed
     * @param context
     * @param mediaObservable
     * @return
     */
    public static Observable<ParseMedia> saveMedia(Context context, Observable<ParseMedia> mediaObservable) {
        return saveMedia(context, mediaObservable, DEFAULT_JPEG_QUALITY,
                DEFAULT_THUMBNAIL_SIZE, DEFAULT_THUMBNAIL_SIZE);
    }

    /**
     * Method used to save media with thumbnail if needed
     * @param context
     * @param mediaObservable
     * @return
     */
    public static Observable<ParseMedia> saveMedia(Context context, Observable<ParseMedia> mediaObservable,
                                                   int quality, int width, int height) {
        return mediaObservable.concatMap(media -> MediaHelper.createThumbnail(context, media, quality, width, height))
                .concatMap(media -> MediaHelper.getMediaCompressObservable(media, quality))
                .doOnNext(media -> ParseObservable.save(media.getThumbnail()))
                .doOnNext(media -> ParseObservable.save(media.getMediaFile()))
                .flatMap(ParseObservable::save);
    }

    @NonNull
    public static Observable<ParseMedia> createThumbnail(Context context, final ParseMedia parseMedia) {
        return createThumbnail(context, parseMedia, DEFAULT_JPEG_QUALITY, DEFAULT_THUMBNAIL_SIZE, DEFAULT_THUMBNAIL_SIZE);
    }

    @NonNull
    public static Observable<ParseMedia> createThumbnail(Context context, final ParseMedia parseMedia,
                                                         int quality, int width, int height) {
        try {
            File file = ((br.com.ilhasoft.support.parse.models.ParseFile)parseMedia.getMediaFile()).getLocalFile();
            if (parseMedia.getType().equals(ParseMedia.TYPE_VIDEO)) {
                Bitmap bitmap = BitmapHelper.getThumbnailFromVideoUri(context, Uri.fromFile(file));
                File createdFile = IoHelper.createImageFilePath(context);

                File thumbnailFile = BitmapCompressor.setBitmapToNewFileCompressed(bitmap, createdFile, quality);
                parseMedia.setThumbnail(new ParseFile(thumbnailFile));
            } else if (parseMedia.getType().equals(ParseMedia.TYPE_PICTURE)) {
                Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getAbsolutePath()),
                                                                width, height);
                bitmap = BitmapHelper.rotateBitmapIfNeeded(bitmap, file);

                File thumbnailFile = IoHelper.createImageFilePath(context);
                BitmapCompressor.setBitmapToNewFileCompressed(bitmap, thumbnailFile, quality);

                parseMedia.setThumbnail(new ParseFile(thumbnailFile));
            }
        } catch(Exception exception) {
            Log.e(TAG, "call: ", exception);
        }
        return Observable.just(parseMedia);
    }

    private static Observable<ParseMedia> getMediaCompressObservable(ParseMedia parseMedia, int quality) {
        Observable<File> compressObservable;
        br.com.ilhasoft.support.parse.models.ParseFile parseFile =
                (br.com.ilhasoft.support.parse.models.ParseFile) parseMedia.getMediaFile();
        if (parseMedia.getType().equals(ParseMedia.TYPE_VIDEO)) {
            compressObservable = FileCompressor.compressVideo(parseFile.getLocalFile()).toObservable();
            return completeMediaFileCompressed(parseMedia, compressObservable);
        } else if (parseMedia.getType().equals(ParseMedia.TYPE_PICTURE)) {
            compressObservable = FileCompressor.compressPicture(parseFile.getLocalFile(), quality).toObservable();
            return completeMediaFileCompressed(parseMedia, compressObservable);
        } else {
            return Observable.just(parseMedia);
        }
    }

    private static Observable<ParseMedia> completeMediaFileCompressed(ParseMedia parseMedia, Observable<File> compressObservable) {
        return compressObservable.flatMap(file -> {
            parseMedia.setMediaFile(new ParseFile(file));
            return Observable.just(parseMedia);
        });
    }

}
