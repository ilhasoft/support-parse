package br.com.ilhasoft.support.parse.listeners;

import com.parse.CountCallback;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class CountListener extends Listener2<Integer> implements CountCallback {

    @Override
    public final void done(int count, ParseException e) {
        this.doneInternal(count, e);
    }

}
