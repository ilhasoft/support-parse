package br.com.ilhasoft.support.parse.models;

/**
 * Created by daniel on 16/06/16.
 */
public class ParseObject extends com.parse.ParseObject {

    public static final String KEY_OBJECT_ID = "objectId";
    public static final String KEY_CREATED_AT = "createdAt";
    public static final String KEY_UPDATED_AT = "updatedAt";

    @Override
    public String toString() {
        return getClass().getName() + "@" + getClassName() + "{objectId='" +
                getObjectId() + "'}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof com.parse.ParseObject)) return false;

        com.parse.ParseObject parseObject = (com.parse.ParseObject) o;

        return getObjectId() != null ? getObjectId().equals(parseObject.getObjectId()) : parseObject.getObjectId() == null;

    }

    @Override
    public int hashCode() {
        return getObjectId() != null ? getObjectId().hashCode() : 0;
    }

    @Override
    public void put(String key, Object value) {
        if (value != null) {
            super.put(key, value);
        } else {
            super.remove(key);
        }
    }
}
