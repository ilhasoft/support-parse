package br.com.ilhasoft.support.parse.listeners;

import com.parse.DeleteCallback;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class DeleteListener extends Listener1 implements DeleteCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
