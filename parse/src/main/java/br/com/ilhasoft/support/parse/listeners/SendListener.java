package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.SendCallback;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class SendListener extends Listener1 implements SendCallback {

    @Override
    public final void done(ParseException e) {
        this.doneInternal(e);
    }

}
