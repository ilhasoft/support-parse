# Support Parse Kotlin

O suporte ao Kotlin já inclui o Support Parse em suas dependências, e traz conceitos existentes em Kotlin para facilitar nossa vida.


## Extension Functions

Hoje usamos [extension functions](https://kotlinlang.org/docs/reference/extensions.html) para todos os métodos do `ParseQuery`, isso nos trás uma certa segurança, vamos a um pequeno exemplo para ficar mais claro:

```kotlin
val query = ParseQuery.getQuery(SomeParseObject::class.java)

// Usamos key como string, isso nos força a mudar essa key
// sempre que houver uma mudança em uma collection do parse.
// E nos podemos esquecer disso facilmente!!
query.whereEqualTo("someKey", value)
```

Com o uso de extension functions, nos passamos a própria propriedade para a chamada dos métodos:

```kotlin
val query = ParseQuery.getQuery(SomeParseObject::class.java)
// title é uma propriedade dentro do objeto SomeParseObject
query.whereEqualTo(SomeParseObject::title, value)
```

Sempre que fizermos uma refatoração nessa propriedade, todos os lugares que usam essa propriedade também receberá a mudança.


## [Property Delegation](https://kotlinlang.org/docs/reference/delegated-properties.html)

> There are certain common kinds of properties, that, though we can implement them manually every time we need them,
> would be very nice to implement once and for all, and put into a library.


O texto citado acima, é a melhor explicação que alguém poderia nos passar, usamos delegation properties para as propriedades de nossos `ParseObject`, isso evita com que tenhamos que escrever uma montanha de boilerplate.

Sem delegation properties:

```kotlin
class Comment : ParseObject {
    private const val BODY_KEY = "body"
    var body: String
        get = getString(BODY_KEY)
        set(value) {
            putString(BODY_KEY, value)
        }
}
```

Com delegation properties:

```kotlin
class Comment : ParseObject {
    val body: String by stringAttribute() // That's it
}
```

`stringAttribute` é um property delegate, e temos pra vários outros tipos, isso fez com que não tivessemos que escrever `get/set` e além disso, removeu o boilerplate do `get/put` que é uma obrigação para mapearmos nossas classes com as collections do Parse

