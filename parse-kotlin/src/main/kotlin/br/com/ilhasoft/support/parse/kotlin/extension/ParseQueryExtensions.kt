package br.com.ilhasoft.support.parse.kotlin.extension

import com.parse.ParseGeoPoint
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlin.reflect.KProperty

/**
 * Created by felipe on 19/03/17.
 */

fun <T : ParseObject> ParseQuery<T>.addAscendingOrder(key: KProperty<Any?>): ParseQuery<T> {
    return addAscendingOrder(key.name)
}

fun <T : ParseObject> ParseQuery<T>.addDescendingOrder(key: KProperty<Any?>): ParseQuery<T> {
    return addDescendingOrder(key.name)
}

fun <T : ParseObject> ParseQuery<T>.include(vararg properties: KProperty<Any?>): ParseQuery<T> {
    val names = mutableListOf<String>()
    properties.forEach { names.add(it.name) }
    return include(names.joinToString("."))
}

fun <T : ParseObject> ParseQuery<T>.orderByAscending(key: KProperty<Any?>): ParseQuery<T> {
    return orderByAscending(key.name)
}

fun <T : ParseObject> ParseQuery<T>.orderByDescending(key: KProperty<Any?>): ParseQuery<T> {
    return orderByDescending(key.name)
}

fun <T : ParseObject> ParseQuery<T>.selectKeys(keys: Collection<KProperty<Any?>>): ParseQuery<T> {
    return selectKeys(keys.map { it.name })
}

fun <T : ParseObject> ParseQuery<T>.whereContainedIn(key: KProperty<Any?>, values: Collection<Any?>): ParseQuery<T> {
    return whereContainedIn(key.name, values)
}

fun <T : ParseObject> ParseQuery<T>.whereContains(key: KProperty<Any?>, substring: String): ParseQuery<T> {
    return whereContains(key.name, substring)
}

fun <T : ParseObject> ParseQuery<T>.whereContainsAll(key: KProperty<Any?>, values: Collection<ParseObject>): ParseQuery<T> {
    return whereContainsAll(key.name, values)
}

fun <T : ParseObject> ParseQuery<T>.whereDoesNotExist(key: KProperty<Any?>): ParseQuery<T> {
    return whereDoesNotExist(key.name)
}

fun <T : ParseObject> ParseQuery<T>.whereDoesNotMatchkeyInQuery(key: KProperty<Any?>, keyInQuery: KProperty<Any?>, query: ParseQuery<ParseObject>): ParseQuery<T> {
    return whereDoesNotMatchKeyInQuery(key.name, keyInQuery.name, query)
}

fun <T : ParseObject> ParseQuery<T>.whereDoesNotMatchQuery(key: KProperty<Any?>, query: ParseQuery<out ParseObject>): ParseQuery<T> {
    return whereDoesNotMatchQuery(key.name, query)
}

fun <T : ParseObject> ParseQuery<T>.whereEndsWith(key: KProperty<Any?>, suffix: String): ParseQuery<T> {
    return whereEndsWith(key.name, suffix)
}

fun <T : ParseObject> ParseQuery<T>.whereEqualTo(key: KProperty<Any?>, value: Any?): ParseQuery<T> {
    return whereEqualTo(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereExists(key: KProperty<Any?>): ParseQuery<T> {
    return whereExists(key.name)
}

fun <T : ParseObject> ParseQuery<T>.whereGreaterThan(key: KProperty<Any?>, value: Any): ParseQuery<T> {
    return whereGreaterThan(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereGreaterThanOrEqualTo(key: KProperty<Any?>, value: Any): ParseQuery<T> {
    return whereGreaterThanOrEqualTo(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereLessThan(key: KProperty<Any?>, value: Any): ParseQuery<T> {
    return whereLessThan(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereLessThanOrEqualTo(key: KProperty<Any?>, value: Any): ParseQuery<T> {
    return whereLessThanOrEqualTo(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereMatches(key: KProperty<Any?>, regex: String): ParseQuery<T> {
    return whereMatches(key.name, regex)
}

fun <T : ParseObject> ParseQuery<T>.whereMatches(key: KProperty<Any?>, regex: String, modifiers: String): ParseQuery<T> {
    return whereMatches(key.name, regex, modifiers)
}

fun <T : ParseObject> ParseQuery<T>.whereMatchesKeyInQuery(key: KProperty<Any?>, keyInQuery: KProperty<Any?>, query: ParseQuery<ParseObject>): ParseQuery<T> {
    return whereMatchesKeyInQuery(key.name, keyInQuery.name, query)
}

fun <T : ParseObject> ParseQuery<T>.whereMatchesQuery(key: KProperty<Any?>, query: ParseQuery<out ParseObject>): ParseQuery<T> {
    return whereMatchesQuery(key.name, query)
}

fun <T : ParseObject> ParseQuery<T>.whereNear(key: KProperty<Any?>, point: ParseGeoPoint): ParseQuery<T> {
    return whereNear(key.name, point)
}

fun <T : ParseObject> ParseQuery<T>.whereNotContainedIn(key: KProperty<Any?>, values: Collection<Any?>): ParseQuery<T> {
    return whereNotContainedIn(key.name, values)
}

fun <T : ParseObject> ParseQuery<T>.whereNotEqualTo(key: KProperty<Any?>, value: Any?): ParseQuery<T> {
    return whereNotEqualTo(key.name, value)
}

fun <T : ParseObject> ParseQuery<T>.whereStartsWith(key: KProperty<Any?>, prefix: String): ParseQuery<T> {
    return whereStartsWith(key.name, prefix)
}

fun <T : ParseObject> ParseQuery<T>.whereWithinGeoBox(key: KProperty<Any?>, southwest: ParseGeoPoint, northeast: ParseGeoPoint): ParseQuery<T> {
    return whereWithinGeoBox(key.name, southwest, northeast)
}

fun <T : ParseObject> ParseQuery<T>.whereWithinKilometers(key: KProperty<Any?>, point: ParseGeoPoint, maxDistance: Double): ParseQuery<T> {
    return whereWithinKilometers(key.name, point, maxDistance)
}

fun <T : ParseObject> ParseQuery<T>.whereWithinMiles(key: KProperty<Any?>, point: ParseGeoPoint, maxDistance: Double): ParseQuery<T> {
    return whereWithinMiles(key.name, point, maxDistance)
}

fun <T : ParseObject> ParseQuery<T>.whereWithinRadians(key: KProperty<Any?>, point: ParseGeoPoint, maxDistance: Double): ParseQuery<T> {
    return whereWithinRadians(key.name, point, maxDistance)
}
