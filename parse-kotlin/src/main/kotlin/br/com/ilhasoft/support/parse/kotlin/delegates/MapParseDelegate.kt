package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class MapParseDelegate<V> {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): MutableMap<String, V> {
        return parseObject.getMap<V>(property.name) ?: mutableMapOf()
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, map: MutableMap<String, V>) {
        parseObject.put(property.name, map)
    }

}

fun <V> mapAttribute() = MapParseDelegate<V>()
