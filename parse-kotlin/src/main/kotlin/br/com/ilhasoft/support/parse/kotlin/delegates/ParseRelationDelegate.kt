package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import com.parse.ParseRelation
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class ParseRelationDelegate<T : ParseObject> {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): ParseRelation<T> {
        return parseObject.getRelation<T>(property.name)
    }

}

fun <T : ParseObject> relationAttribute() = ParseRelationDelegate<T>()

