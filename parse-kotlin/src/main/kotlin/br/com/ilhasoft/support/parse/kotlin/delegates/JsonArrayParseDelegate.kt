package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import org.json.JSONArray
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class JsonArrayParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): JSONArray? {
        return parseObject.getJSONArray(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, jsonArray: JSONArray) {
        parseObject.put(property.name, jsonArray)
    }

}

fun jsonArrayAttribute() = JsonArrayParseDelegate()
