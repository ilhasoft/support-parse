package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class BooleanParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): Boolean {
        return parseObject.getBoolean(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, boolean: Boolean) {
        parseObject.put(property.name, boolean)
    }

}

fun booleanAttribute() = BooleanParseDelegate()
