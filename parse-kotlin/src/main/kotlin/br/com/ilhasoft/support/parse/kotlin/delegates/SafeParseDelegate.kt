package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import org.json.JSONObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class SafeParseDelegate<T> {

    @Suppress("UNCHECKED_CAST")
    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): T? {
        val value = if (parseObject.has(property.name)) {
            parseObject.get(property.name)
        } else {
            null
        }
        return if (JSONObject.NULL == value) {
            null
        } else {
            value as? T?
        }
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, t: T?) {
        if (t != null) {
            parseObject.put(property.name, t)
        } else {
            try {
                parseObject.remove(property.name)
            } catch (e: IllegalStateException) { }
        }
    }

}

fun <T> safeAttribute() = SafeParseDelegate<T>()
