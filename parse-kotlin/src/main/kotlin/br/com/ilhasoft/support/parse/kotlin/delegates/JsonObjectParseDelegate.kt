package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import org.json.JSONObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class JsonObjectParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): JSONObject? {
        return parseObject.getJSONObject(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, jsonObject: JSONObject) {
        parseObject.put(property.name, jsonObject)
    }

}

fun jsonObjectAttribute() = JsonObjectParseDelegate()
