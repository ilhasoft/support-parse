package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class ListParseDelegate<T> {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): MutableList<T> {
        return parseObject.getList(property.name) ?: mutableListOf()
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, list: MutableList<T>) {
        parseObject.put(property.name, list)
    }

}

fun <T> listAttribute() = ListParseDelegate<T>()
