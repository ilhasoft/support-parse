package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class DoubleParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): Double {
        return parseObject.getDouble(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, double: Double) {
        parseObject.put(property.name, double)
    }

}

fun doubleAttribute() = DoubleParseDelegate()
