package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class IntParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): Int {
        return parseObject.getInt(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, int: Int) {
        parseObject.put(property.name, int)
    }

}

fun intAttribute() = IntParseDelegate()
