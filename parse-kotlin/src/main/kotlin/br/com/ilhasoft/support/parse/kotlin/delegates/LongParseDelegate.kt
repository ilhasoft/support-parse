package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class LongParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): Long {
        return parseObject.getLong(property.name)
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, long: Long) {
        parseObject.put(property.name, long)
    }

}

fun longAttribute() = LongParseDelegate()
