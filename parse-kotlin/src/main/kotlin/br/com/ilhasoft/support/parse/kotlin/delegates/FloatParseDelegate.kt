package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class FloatParseDelegate {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): Float {
        return parseObject.getDouble(property.name).toFloat()
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, float: Float) {
        parseObject.put(property.name, float)
    }

}

fun floatAttribute() = FloatParseDelegate()
