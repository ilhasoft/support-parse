package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 29/11/16.
 */
class ParseDelegate<T> {

    @Suppress("UNCHECKED_CAST")
    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): T {
        return parseObject.get(property.name) as T
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, t: T?) {
        if (t != null) parseObject.put(property.name, t)
    }

}

fun <T> attribute() = ParseDelegate<T>()
