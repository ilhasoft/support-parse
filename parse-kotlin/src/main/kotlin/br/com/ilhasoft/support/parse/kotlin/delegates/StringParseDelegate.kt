package br.com.ilhasoft.support.parse.kotlin.delegates

import com.parse.ParseObject
import kotlin.reflect.KProperty

/**
 * Created by daniel on 05/05/17.
 */
class StringParseDelegate(private val filter: (String) -> String) {

    operator fun getValue(parseObject: ParseObject, property: KProperty<*>): String {
        return parseObject.getString(property.name) ?: ""
    }

    operator fun setValue(parseObject: ParseObject, property: KProperty<*>, string: String?) {
        string?.let { parseObject.put(property.name, filter.invoke(it)) }
    }

}

fun stringAttribute(filter: (String) -> String = { it }) = StringParseDelegate(filter)
